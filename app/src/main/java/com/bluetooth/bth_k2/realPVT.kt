package com.bluetooth.bth_k2

import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates


class stimu{
    //var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS")

    var index by Delegates.notNull<Int>()
    var ISI by Delegates.notNull<Int>()
    var LorR by Delegates.notNull<Int>()
    var RT by Delegates.notNull<Int>()
    var correct by Delegates.notNull<Int>()

    fun stimu(){
        index = -1
        ISI = 0
        RT = 0
        LorR = -1
        correct = 1
    }
}

class realPVT : AppCompatActivity() {

////////PVT 實驗時間
    var min = 3
////////PVT 實驗時間
//////時間暫停器
var postclickable:Int = 3
//////時間暫停器
    //private lateinit var bManager : BluetoothManager
    //private lateinit var bAdapter : BluetoothAdapter
    //var mgatt: BluetoothGatt? = null

    private var DINDEX = 0
    var name:String = "Tester"

/*
    ///service define
    val UUID_SERIVCE =      "1234E001-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_ECG =     "1234E002-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_TIME =    "1234E005-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_COMMAND = "1234E004-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_BCG = "1234E006-FFFF-1234-FFFF-111122223333"
    ////service define
    lateinit var biologue_service: BluetoothGattService
    lateinit var biologue_char_ecg: BluetoothGattCharacteristic
    lateinit var biologue_char_time: BluetoothGattCharacteristic
    lateinit var biologue_char_command: BluetoothGattCharacteristic
    lateinit var biologue_char_bcg: BluetoothGattCharacteristic

    val COMMAND_ECG_START = byteArrayOf(0xAA.toByte(), 0xF0.toByte(), 0xAA.toByte())
    val COMMAND_ECG_STOP = byteArrayOf(0xAA.toByte(), 0xF1.toByte(), 0xAA.toByte())
    private var presc_cnt = 0
    private val PRESC_COUNT = 10
    private var ecg_data_count: ULong = 0UL
    var descript_write = false
*/

    private val ECG_DATA_DIRECTORY = "test"
    private var storagePath: File? = null
    //private lateinit var DefaultECGName: String
    //private lateinit var DefaultBCGName: String
    private lateinit var DefaultPVTName: String
    var stage_change_name : String = "Tmp_stage_change.csv"



    //lateinit var tv_hi:TextView

    var responseList = arrayListOf<stimu>()
    var stiINDEX = 0
    var hi_exist = false
///////////time count
    var timerHandler3: Handler? = Handler()
    var totaltime = 0
    var nowtime = 0
    var epochtime =0

    var starttime by Delegates.notNull<Long>()
    var endtime by Delegates.notNull<Long>()

///////////time count

    private lateinit var fullscreenContent: TextView
    private lateinit var fullscreenContentControls: LinearLayout
    private val hideHandler = Handler()
    private var stage  =0;
    private var preKSS =0;
    private var postKSS =0;

    var waketime = 0



    private val timerRunnable3: Runnable = object : Runnable {
        override fun run() {
            runOnUiThread {
                totaltime += 1
                nowtime += 1

                if(epochtime <= nowtime && !hi_exist && stage < 3){
                    hi_exist = true
                    fullscreenContent.text = "HI!!"

                    starttime = Date().time
                    //Log.e("starttime",starttime.toString() )
                    //dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
                    //txv_time.setText(dff.format(Date()))
                }///////produce next hi

                if(totaltime > 60* min && stage == 1){
                    if(!hi_exist){
                        hi_exist = true
                        fullscreenContent.text = "HI!!"

                        starttime = Date().time
                        //Log.e("starttime",starttime.toString() )
                    }
                    stage = 2
                    Log.e("stage","2")

                    //timerRunnable3.
                }/////PVT test over
                if(postclickable !=0 && stage ==3)postclickable -= 1



            }

            timerHandler3!!.postDelayed(this, 1000)

        }
    }




    var KSS_cont = arrayListOf(
            "1.  極度警覺",
            "2.  很警覺",
            "3.  警覺",
            "4.  有點警覺",
            "5.  既不警覺也不想睡",
            "6.  有一些睡意",
            "7.  想睡，但不需特別努力維持清醒",
            "8.  想睡，需花費一些努力維持清想",
            "9.  非常想睡，需花費很大的力氣維持清醒",
            "10. 極度想睡，無法保持清醒")


    @SuppressLint("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        fullscreenContent.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        fullscreenContentControls.visibility = View.VISIBLE
    }
    private var isFullscreen: Boolean = false

    private val hideRunnable = Runnable { hide() }

    lateinit var bt_react :Button
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    /*
    private val delayHideTouchListener = View.OnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS)
            }
            MotionEvent.ACTION_UP -> view.performClick()
            else -> {
            }
        }
        false
    }
       */
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        postclickable = 5

        //tv_hi = findViewById(R.id.tv_hi)

        //bManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        //bAdapter = bManager?.adapter

        preKSS = intent.getIntExtra("KSS", 0) + 1
        DINDEX = intent.getIntExtra("device", 0)
        name = intent.getStringExtra("name")!!
        waketime = intent.getIntExtra("waketime", 7)

        stage_change_name = intent.getStringExtra("stagechange")!!

        Log.e("startPVT", DINDEX.toString())
        create_saving_directory()


        setContentView(R.layout.activity_real_p_v_t)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        isFullscreen = true

        // Set up the user interaction to manually show or hide the system UI.
        fullscreenContent = findViewById(R.id.tv_hi)
        fullscreenContent.setOnClickListener { toggle() }

        fullscreenContentControls = findViewById(R.id.fullscreen_content_controls)

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        bt_react = findViewById(R.id.dummy_button)

        storagePath = this.getExternalFilesDir(null)



    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }
/*
    fun send_start() {
        if (biologue_char_command != null) {
            biologue_char_command.setValue(COMMAND_ECG_START)
            var ch_cmd = mgatt!!.writeCharacteristic(biologue_char_command)

            if(ch_cmd)Log.e("sendcommand", "start_send")
            else Log.e("sendcommand", "start_sendfailed!!")
        }
        else{
            Log.e("sendcommand", "cmd is null")
        }
    }
    fun send_stop() {

        if (biologue_char_command != null) {
            biologue_char_command.setValue(COMMAND_ECG_STOP)
            var ch_cmd =  mgatt!!.writeCharacteristic(biologue_char_command)
            if(ch_cmd) Log.e("sendcommand", "stop_send")
            else Log.e("sendcommand", "stop_sendfailed!!")
        }

        else{
            Log.e("sendcommand", "cmd is null")
        }
    }
*/
    fun tryDiCript(cypher: ByteArray) :String {
        //var cypher = "fs2543i435u@$#g#@#sagb@!#12416@@@"
        var originalText = ""

        //var regEx =Regex("[a-z]")
        for(tmp in cypher)
        {
            originalText += tmp.toUByte().toString() + ","
        }
        originalText.dropLast(1)
        originalText += "\n"

        return originalText
    }


    fun create_saving_directory() {
        var dataDir = File(storagePath, ECG_DATA_DIRECTORY)
        if(dataDir.mkdirs())Log.e("mkdir", dataDir.toString())
    }


    fun writeToStorage(fileName: String, data: ByteArray) {
        var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$fileName")
        //var outSt = FileOutputStream(extFile, true)
        val outdata =  tryDiCript(data)
        //outSt.write(data)
        //outSt.close()
        extFile.appendText(outdata)
        //extFile
    }

    ///////////touchscreen
    fun touchscreen(view: View) {
        if(stage == 0){
            fullscreenContent.text = ""
/*
            Thread {
                var d = bManager.getConnectedDevices(BluetoothProfile.GATT)[DINDEX]
                mgatt = d.connectGatt(this, false, gattCB)
                Thread.sleep(2000)
                send_start()
            }.start()/////////connect thread
  */
            Thread {
                timerHandler3?.postDelayed(timerRunnable3, 0)
            }.start()////////refresh timer thread

            epochtime =( (Math.random() *8) + 2).toInt()
            //val dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            //dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            //DefaultECGName = dff.format(Date()) + ".txt"
            //DefaultBCGName = dff.format(Date()) + "_BCG.txt"
            //DefaultPVTName = "PVT_"+ name +"_"+ dff.format(Date()) +".csv"
            //Log.e("File", DefaultECGName)
            //Log.e("File", DefaultBCGName)
            //Log.e("File", DefaultPVTName)
            stage = 1
        }/////start PVT test
        else if(stage == 1 && hi_exist){
            endtime = Date().time


            fullscreenContent.text = ""
            epochtime = ( (Math.random() *8) + 2).toInt()
            nowtime = 0

            var res = stimu()

            res.index = stiINDEX
            res.ISI = epochtime*1000
            res.LorR = -1
            res.RT = (endtime - starttime).toInt()
            res.correct =1

            stiINDEX++

            hi_exist = false
            responseList.add(res)

        }/////PVT test running
        else if(stage == 2){
            endtime = Date().time


            //Thread {
                fullscreenContent.text = "選擇現在的KSS分數  然後點擊畫面下半部結束測試"
                var res = stimu()


                var st_ms = starttime
                var ed_ms = endtime

                res.index = stiINDEX
                res.ISI = epochtime * 1000
                res.LorR = -1
                res.RT = (endtime - starttime).toInt()
                res.correct = 1

                stiINDEX++

                hi_exist = false
                responseList.add(res)
                stage = 3
                Log.e("stage", "3")


                val pKSSspin: Spinner = findViewById(R.id.postKSSspinner)
                val adapter2 = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, KSS_cont)

                pKSSspin.adapter = adapter2
                pKSSspin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        Log.e("Spinner", "select $p2")
                        DINDEX = p2
                    }

                   override fun onNothingSelected(p0: AdapterView<*>?) {
                        Log.e("Spinner", "Nothing select")
                    }
                }

                pKSSspin.visibility = View.VISIBLE
            //}.start()
                    //Systemclock.sleep(2000);




        }//////stop PVT test  and start postKSS
        else if(stage == 3 && postclickable == 0){
            postKSS = DINDEX +1;
            Log.e("stage","4")

            var dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            DefaultPVTName = "PVT_"+ name +"_"+ dff.format(Date()) +".csv"
            Log.e("File", DefaultPVTName)




            var dffhour  = SimpleDateFormat("HH")
            dffhour.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            var nowhour = dffhour.format(Date()).toInt() - waketime

            while(nowhour<0)nowhour +=24

            ////////////write PVT　data result
            var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$DefaultPVTName")

            extFile.appendText("WHE_TEST_version, 0.1.0.m \n")
            extFile.appendText("sleep_time, "+"8" +"\n")
            extFile.appendText("wake_time, "+ nowhour +"\n")
            extFile.appendText("sleep_2h_time, "+"0" +"\n")
            extFile.appendText("HR, 0\n")
            extFile.appendText("SSS, 0\n")
            extFile.appendText("SSS_post, 0\n")
            extFile.appendText("KSS, "+ preKSS.toString() +"\n")
            extFile.appendText("KSS_post, "+postKSS.toString() +"\n")
            extFile.appendText("index, ISI, LorR, RT, correct\n")
            for(ru in responseList){
                extFile.appendText(ru.index.toString() + ", "+ru.ISI.toString()+", "+ru.LorR.toString()+", "+ ru.RT.toString() +", " + ru.correct.toString() + "\n")
            }
            ////////////write PVT　data result

            ////////////write stage change
            extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$stage_change_name")

            dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))

            extFile.appendText(dff.format(Date())+"\n")
            //////////////write stage change
            stage =4;
            finish()
        }//////select postKSS and save PVT result
        else{


        }
    }
//////////touchscreen


    private fun toggle() {
        if (isFullscreen) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        fullscreenContentControls.visibility = View.GONE
        isFullscreen = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        hideHandler.removeCallbacks(showPart2Runnable)
        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        // Show the system bar
        fullscreenContent.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        isFullscreen = true

        // Schedule a runnable to display UI elements after a delay
        hideHandler.removeCallbacks(hidePart2Runnable)
        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }
/*
    private val gattCB = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
                gatt: BluetoothGatt,
                status: Int,
                newState: Int
        ) {
            Log.e("GATT", "onConnectStateChange")
            //if() {
            //    Bt_BCG?.text = "connect"
            //    bcg_collect_stage = false
            //}
            gatt?.discoverServices()
        }
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            val intentAction: String
            Log.e("GATT", "onServicesDiscovered")


            biologue_service = gatt!!.getService(UUID.fromString(UUID_SERIVCE))

            //Log.e("Biologue", "Found service")
            // Get Characteristic
            biologue_char_ecg = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_ECG))
            biologue_char_time = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_TIME))

            biologue_char_command = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_COMMAND))

            biologue_char_bcg = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_BCG))

            Log.e("serviceDiscovered", biologue_char_command.toString())

            // Enable Notify ECG
            var notify_success = gatt!!.setCharacteristicNotification(biologue_char_ecg, true)
            if(notify_success) Log.e("Biologue", "Enable notify 1")
            else Log.e("Biologue", "Fail to enable notify 1")

            notify_success = gatt!!.setCharacteristicNotification(biologue_char_bcg, true)
            if(notify_success) Log.e("Biologue", "Enable notify 2")
            else Log.e("Biologue", "Fail to enable notify 2")


            for (dp in biologue_char_ecg.getDescriptors()) {
                Log.e("gattdevice-ecg", "dp:" + dp.toString())
                if (dp != null) {
                    if (biologue_char_ecg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0) {
                        dp.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    }
                    else if (biologue_char_ecg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_INDICATE != 0 ) {
                        dp.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                    }
                    //descript_write = false
                    var tmp = gatt.writeDescriptor(dp)
                    Log.e("ECG", tmp.toString())

                }
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            Log.e("DW", gatt.toString())
            if(!descript_write) {
                for (dp in biologue_char_bcg.getDescriptors()) {
                    Log.e("gattdevice-bcg", "dp:" + dp.toString())
                    if (dp != null) {
                        if (biologue_char_bcg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0) {
                            dp.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                        } else if (biologue_char_bcg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_INDICATE != 0) {
                            dp.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                        }
                        //descript_write = false
                        var tmp = mgatt!!.writeDescriptor(dp)
                        Log.e("BCG", tmp.toString())
                        //Thread.sleep(1000)
                        //send_start()
                        //while(!descript_write){ PERMISSION_REQUEST_STORAGE = PERMISSION_REQUEST_STORAGE }
                    }
                }
            }
            descript_write = true
        }


        override fun onCharacteristicChanged(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?
        ){

            var ecgData = characteristic!!.value
            if (characteristic.uuid == UUID.fromString(UUID_CHAR_ECG)) {
                if(presc_cnt % PRESC_COUNT == 0) {
                    Log.e("Biologue", "ECG data in " + ecgData?.size.toString())
                }
                // txv_record.setText(ecg_data_count.toString())
                // txv_record.setText(characteristic.toString())
                presc_cnt++
                ecg_data_count++
                //Log.e("FFFF", "XXXXXXX")
                /* Save data */
                writeToStorage(DefaultECGName, ecgData)
            }
            else if(characteristic.uuid == UUID.fromString(UUID_CHAR_BCG)){

                if(presc_cnt % PRESC_COUNT == 0) {
                    Log.e("Biologue", "BCG data in " + ecgData?.size.toString())
                }
                // txv_record.setText(ecg_data_count.toString())
                // txv_record.setText(characteristic.toString())
                presc_cnt++
                ecg_data_count++
                //Log.e("FFFF", "XXXXXXX")
                /* Save data */
                writeToStorage(DefaultBCGName, ecgData)
            }
        }



        override fun onCharacteristicRead(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?,
                status: Int){


            Log.e("GATT", "READ")

            var recData: ByteArray = characteristic!!.value
            //if(recData.size == 6) {
            var unixTIme: ULong = recData[3].toUByte().toULong().shl(24) + recData[2].toUByte().toULong().shl(16) +
                    recData[1].toUByte().toULong().shl(8) + recData[0].toUByte().toULong()
            Log.e(
                    "data",
                    recData[0].toUByte().toString() + "," + recData[1].toUByte()
                            .toString() + "," + recData[2].toUByte()
                            .toString() + "," + recData[3].toUByte().toString() + "," +
                            recData[4].toUByte().toString() + "," + recData[5].toUByte()
                            .toString() + ","
            )
            //}
        }
        override fun onCharacteristicWrite(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int){
            Log.e("GATT", "WRITE " + characteristic.toString() + status.toString())
        }
    }
    */
}