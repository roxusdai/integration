package com.bluetooth.bth_k2

import android.R.string
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class PVT_menu : AppCompatActivity() {


    var KSS_cont = arrayListOf(
            "1.  極度警覺",
            "2.  很警覺",
            "3.  警覺",
            "4.  有點警覺",
            "5.  既不警覺也不想睡",
            "6.  有一些睡意",
            "7.  想睡，但不需特別努力維持清醒",
            "8.  想睡，需花費一些努力維持清想",
            "9.  非常想睡，需花費很大的力氣維持清醒",
            "10. 極度想睡，無法保持清醒")

    private lateinit var bluetoothManager : BluetoothManager
    private lateinit var bluetoothAdapter : BluetoothAdapter
    var gattlist: List<String> = ArrayList()
    var devicemac : String = ""
    var name : String = "Tester"
    var stage_change : String = "Tmp_stage_change.csv"
    var waketime : Int = 0

    lateinit var Tirespin : Spinner
    lateinit var ECGspin : Spinner
    lateinit var tv_name : TextView

    var ECGINDEX = 0
    var TireINDEX= 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p_v_t_menu)

        Tirespin = findViewById(R.id.Fatiguespin)
        ECGspin = findViewById(R.id.Devicespin)
        tv_name = findViewById(R.id.txv_name)
        name = intent.getStringExtra("name")!!

        tv_name.setText(name)

        stage_change = "State_Change_List_"+name+".csv"
        //stage_change = intent.getStringExtra("stagechange")!!
        waketime = intent.getIntExtra("waketime",waketime)


////////////////////////////ECG list
        bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager?.adapter
        Log.e("GATT device", bluetoothManager.getConnectedDevices(BluetoothProfile.GATT).toString())
        for(g in bluetoothManager.getConnectedDevices(BluetoothProfile.GATT)) gattlist += (g.name.toString() + g.toString() )

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, gattlist)
        ECGspin.adapter = adapter
        ECGspin.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select $p2")
                ECGINDEX = p2
                devicemac = gattlist.get(ECGINDEX)

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Log.e("Spinner", "Nothing select")
            }
        }
//////////////////////ECG list


//////////////////////KSS list
        val adapter2 = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, KSS_cont )

        Tirespin.adapter = adapter2
        Tirespin.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select $p2")
                TireINDEX = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Log.e("Spinner", "Nothing select")
            }
        }
/////////////////////KSS list


    }

    fun startclick(view: View) {

        if(devicemac == null){
            Toast.makeText(this, "Please Connect a device!!", Toast.LENGTH_LONG).show()
        }
        else {

            val intent = Intent(this, realPVT::class.java)
            intent.putExtra("device",ECGINDEX)
            intent.putExtra("KSS", TireINDEX)
            intent.putExtra("name",name)
            intent.putExtra("stagechange",stage_change)
            intent.putExtra("waketime",waketime)
            startActivity(intent)

        }
    }


    fun backtomain(view: View) {
            finish()
            //val intent = Intent(this, MainActivity::class.java)
            //startActivity(intent)
    }

}