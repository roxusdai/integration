package com.bluetooth.bth_k2

import android.Manifest
import android.bluetooth.*
import android.bluetooth.BluetoothProfile.GATT
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.experimental.or


class ECG : AppCompatActivity() {
    private lateinit var bluetoothManager : BluetoothManager
    private lateinit var bluetoothAdapter : BluetoothAdapter
    //private lateinit var device : BluetoothDevice
    var READ_REQUEST_CODE = 42 //request file code
    var progress_count = 0
    //var PERMISSION_REQUEST_STORAGE = 1000
    var BLEM: BLEManager? = null
    var TAG = "ECG"
    var filesize: Long = 0
    var fs: FileInputStream? = null
    lateinit var ota_binary_data : ByteArray
    var pg_bar: ProgressBar? = null

    ///service define
    val UUID_SERIVCE =      "1234E001-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_ECG =     "1234E002-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_TIME =    "1234E005-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_COMMAND = "1234E004-FFFF-1234-FFFF-111122223333"
    val UUID_CHAR_BCG = "1234E006-FFFF-1234-FFFF-111122223333"
    ////service define
    lateinit var biologue_service: BluetoothGattService
    lateinit var biologue_char_ecg: BluetoothGattCharacteristic
    lateinit var biologue_char_time: BluetoothGattCharacteristic
    lateinit var biologue_char_command: BluetoothGattCharacteristic
    lateinit var biologue_char_bcg: BluetoothGattCharacteristic


    //lateinit var test_char:BluetoothGattCharacteristic
    var mgatt: BluetoothGatt? = null

    private val hdlr = Handler()

    lateinit var txv_record: TextView
    lateinit var Timestamp_text: TextView
    lateinit var spinr : Spinner
    lateinit var spinr2 : Spinner

    var Bt_ECG: Button? = null
    var Bt_trans: Button? = null
    //var TV_verify: TextView? = null
    //var TV_verifyBCG: TextView? = null
    var PB_verifyECG : ProgressBar? = null
    var PB_verifyBCG :ProgressBar? = null

    private var presc_cnt = 0
    //private var ecg_data_count: ULong = 0UL


    //var bcg_collect_stage = false
    var ecg_collect_stage = false
    var trnsfr_stage  =  false

    var descript_write = false

    var ota_times = 0
    var sha256key = byteArrayOf(
            0x49.toByte(), 0x10.toByte(),
            0x25.toByte(), 0x4a.toByte(), 0x2a.toByte(), 0x6e.toByte(), 0xe0.toByte(), 0xe3.toByte(), 0xd5.toByte(), 0x6d.toByte(),
            0x75.toByte(), 0x95.toByte(), 0xb8.toByte(), 0x5d.toByte(), 0x85.toByte(), 0xfc.toByte(), 0xee.toByte(), 0x7.toByte(),
            0x7b.toByte(), 0xd5.toByte(), 0xcc.toByte(), 0x5f.toByte(), 0x4.toByte(), 0xc7.toByte(), 0x9e.toByte(), 0x66.toByte(),
            0x5a.toByte(), 0x49.toByte(), 0x3.toByte(), 0x21.toByte(), 0x14.toByte(), 0x42.toByte(), 0x99.toByte(), 0x83.toByte(),
            0x1c.toByte(), 0x81.toByte(), 0x97.toByte(), 0x45.toByte(), 0xb2.toByte(), 0xd8.toByte(), 0x6c.toByte(), 0x31.toByte(),
            0xc5.toByte(), 0x70.toByte(), 0x62.toByte(), 0x35.toByte(), 0xff.toByte(), 0xb2.toByte(), 0x78.toByte(), 0xae.toByte(),
            0xe2.toByte(), 0x49.toByte(), 0xd9.toByte(), 0x93.toByte(), 0x3c.toByte(), 0x54.toByte(), 0xd9.toByte(), 0x47.toByte(),
            0xd0.toByte(), 0xe2.toByte(), 0x16.toByte(), 0x17.toByte(), 0x12.toByte(), 0x3d.toByte(), 0x8.toByte(), 0xa3.toByte())

    val COMMAND_ECG_START = byteArrayOf(0xAA.toByte(), 0xF0.toByte(), 0xAA.toByte())
    val COMMAND_ECG_STOP = byteArrayOf(0xAA.toByte(), 0xF1.toByte(), 0xAA.toByte())
    var gattlist: List<String> = ArrayList()


    private var storagePath: File? = null
   // private lateinit var today :
    private val ECG_DATA_DIRECTORY = "ECG_DATA"
    private lateinit var DefaultFileName: String
    private lateinit var DefaultBCGName: String

    private var CMD_INDEX = 0
    private var CMD_INDEX2 =0
/*
    fun tryDiCript(cypher: IntArray) :String {
        //var cypher = "fs2543i435u@$#g#@#sagb@!#12416@@@"
        var originalText = ""

        //var regEx =Regex("[a-z]")
        for(tmp in cypher)
        {
            originalText += tmp.toString() + ","
        }
        originalText.dropLast(1)
        originalText += "\n"

        return originalText
    }
*/
   val myThread = object : Thread() { // a potentially time consuming task

       override fun run() {
            super.run()
            progress_count = 0
           Log.e("Thread", "thread started")


           while (progress_count < 100 && BLEM!!.isConnected()) {
                val startTime = System.nanoTime()
                //progress_count = 0
                if (progress_count < 100) hdlr.post { update_progress(progress_count) }
                ota_upgrade()
                Log.e("Measure", "TASK took : " + java.lang.Long.toString((System.nanoTime() - startTime) / 1000000) + "ms")
                //Timestamp_text.setText("Time duration:" + (System.nanoTime() - startTime) / 1000000 + "ms")
                try {
                    sleep(100)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }

            if(BLEM!!.isConnected())BLEM!!.Disconnect()
            hdlr.post { Bt_trans?.text = "transfer" }
            trnsfr_stage = false
        }
    }

    //var ECG_ID: Long = 0
    //val trans_ID: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_e_c_g)
        Log.e("onCreate", ecg_collect_stage.toString())

        val intent = intent
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        spinr = findViewById(R.id.Tspin)
        spinr2 = findViewById(R.id.ECGspin)
        txv_record = findViewById(R.id.tx_record)
        Bt_ECG = findViewById(R.id.readECG)
        Bt_trans = findViewById(R.id.transfer1)

        pg_bar = findViewById(R.id.progressBar3)
        PB_verifyBCG =  findViewById(R.id.PB_BCG)
        PB_verifyECG = findViewById(R.id.PB_ECG)

        //TV_verify = findViewById(R.id.tx_verify)
        //TV_verifyBCG = findViewById(R.id.tx_verifyBCG)


        Timestamp_text = findViewById(R.id.Timestp)

        bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager?.adapter

        //android::alwaysRetainTaskState

        ecg_collect_stage = intent.getBooleanExtra("ECG_connect",false)

        if(ecg_collect_stage){

        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e("Permission", "Request External Storage")
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    9
            )
        }
        // Here, thisActivity is the current activity
        global.getInstance().init(this)
        BLEM = global.getInstance().BLEMan

        Log.e("GATT device", bluetoothManager.getConnectedDevices(GATT).toString())
        for(g in bluetoothManager.getConnectedDevices(GATT)) gattlist += (g.name.toString() + g.toString() )

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, gattlist)
        spinr.adapter = adapter

        spinr.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select $p2")
                CMD_INDEX = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Log.e("Spinner", "Nothing select")
            }
        }
        val adapter2 = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, gattlist)
        spinr2.adapter = adapter2

        spinr2.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select $p2")
                CMD_INDEX2 = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Log.e("Spinner", "Nothing select")
            }
        }

        storagePath = this.getExternalFilesDir(null)

        val dff = SimpleDateFormat("yyyyMMdd_HH:mm")
        dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
        DefaultFileName = dff.format(Date()) + ".txt"
        DefaultBCGName = dff.format(Date()) + "_BCG.txt"
        Log.e("File", storagePath.toString())
        Log.e("File", DefaultFileName)
        Log.e("File", DefaultBCGName)
        create_saving_directory()
    }

    override fun onStop() {
        super.onStop()
        Log.e("onstop", "re:0")
        //finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("onDestory", "re:0")
        //finish()
    }


    //override fun on

    override fun onResume() {
        super.onResume()
        Log.e("onResume", "re:0")

    }

        override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.e("onSaveInstanceState", "saving")
        outState.putBoolean("ecg_collect_stage", ecg_collect_stage)
        //outState.putInt("progress_count", progress_count)
        outState.putString("txv_record", "restore successful")
        //outState.putString("")

        //if(mgatt != null){
        //    outState.putString("gattmac", mgatt?.device.toString())
        //}
        //outState.put
    }

    private fun get_file_name_from_uri(uri: Uri): String? {
        val returnCursor = contentResolver.query(uri, null, null, null, null)
        val nameIndex = returnCursor!!.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        return returnCursor.getString(nameIndex)
    }
    private fun get_file_size_from_uri(fileUri: Uri): Long {
        val returnCursor = contentResolver.query(fileUri, null, null, null, null)
        val sizeIndex = returnCursor!!.getColumnIndex(OpenableColumns.SIZE)
        returnCursor.moveToFirst()
        return returnCursor.getLong(sizeIndex)
    }
    @Throws(IOException::class)
    private fun get_binary_data_from_uri(uri: Uri): String? {
        val stringBuilder = StringBuilder()
        fs = FileInputStream(contentResolver.openFileDescriptor(uri, "r")!!.fileDescriptor) //read only
        filesize = get_file_size_from_uri(uri)
        val bytearr = ByteArray(filesize.toInt())
        if (fs != null) {
            fs!!.read(bytearr, 0, filesize.toInt())
            ota_binary_data = bytearr
        }
        return stringBuilder.toString()
    }


    fun ota_key_cmd() {

        //byte[] data = sha256key;
        BLEM!!.UART_Writebytes(sha256key)
    }

    fun ota_start_cmd(addr: Int, len: Int) {
        val pack = ByteArray(10)
        pack[0] = 0x50.toByte()
        pack[1] = 0x08.toByte()
        pack[2] = (len shr 0).toByte()
        pack[3] = (len shr 8).toByte()
        pack[4] = (len shr 16).toByte()
        pack[5] = (len shr 24).toByte()
        pack[6] = (addr shr 0).toByte()
        pack[7] = (addr shr 8).toByte()
        pack[8] = (addr shr 16).toByte()
        pack[9] = (addr shr 24).toByte()
        BLEM!!.UART_Writebytes(pack)
    }

    fun ota_write_cmd(pdata: ByteArray) {
        val pack = ByteArray(pdata.size + 2)
        pack[0] = 0x51
        pack[1] = (pdata.size and 0xff).toByte()
        for (i in 2 until pack.size) {
            pack[i] = pdata[i - 2]
        }
        BLEM!!.UART_Writebytes(pack)
    }

    fun ota_verify_cmd(check: Int) {

        //byte[] data={(byte)0x52,(byte)0x01,(byte)0x43,(byte)0x12, (byte) 0xab, (byte) 0xcd};
        val data = ByteArray(6)
        data[0] = 0x52.toByte()
        data[1] = 0x01.toByte()
        data[2] = (check shr 0).toByte()
        data[3] = (check shr 8).toByte()
        data[4] = (check shr 16).toByte()
        data[5] = (check shr 24).toByte()
        BLEM!!.UART_Writebytes(data)
    }

    fun ota_clearrom_cmd(start_addr: Int, end_addr: Int) {
//        byte[] data={(byte)0x53,(byte)0x08,(byte)0x00,(byte)0x00, (byte) 0x05, (byte) 0x00
//                ,(byte)0x00,(byte)0x00, (byte) 0x07, (byte) 0x00};
        val data = ByteArray(10)
        data[0] = 0x53
        data[1] = 0x08
        data[2] = (start_addr shr 0).toByte()
        data[3] = (start_addr shr 8).toByte()
        data[4] = (start_addr shr 16).toByte()
        data[5] = (start_addr shr 24).toByte()
        data[6] = (end_addr shr 0).toByte()
        data[7] = (end_addr shr 8).toByte()
        data[8] = (end_addr shr 16).toByte()
        data[9] = (end_addr shr 24).toByte()
        BLEM!!.UART_Writebytes(data)
    }

    fun ota_flash_cmd() {
        val data = byteArrayOf(0x54.toByte(), 0x01.toByte(), 0x43.toByte(), 0x12.toByte(), 0xab.toByte(), 0xcd.toByte())
        BLEM!!.UART_Writebytes(data)
    }

    fun ota_check_version() {
        val data = ByteArray(2)
        data[0] = 0x55.toByte()
        data[1] = 0x01.toByte()
        BLEM!!.UART_Writebytes(data)
    }

    fun byte_sum(vararg bytes: Byte): Byte {
        var total: Byte = 0
       // for (b in bytes) {
            total = bytes.sum().toByte()
       // }
        return total
    }

    fun ota_write_page(address: Int, page: ByteArray) {
        ota_start_cmd(address, page.size)
        var remain_size = page.size
        var count = 0
        val pack_size = 128 //default 128
        while (remain_size > 0) {
            if (remain_size >= pack_size) {
                ota_write_cmd(Arrays.copyOfRange(page, count, count + pack_size))
                count += pack_size
                remain_size -= pack_size
            } else {
                ota_write_cmd(Arrays.copyOfRange(page, count, count + remain_size))
                count += remain_size
                remain_size = 0
            }
        }
        ota_flash_cmd()
        val checksum = byte_sum(*page)
        Log.e("checksum", "checksum = " + java.lang.Byte.toString(checksum))
        ota_verify_cmd(checksum.toInt())
    }

    fun checksum_error(data: ByteArray): Int {
        var sum = 0
        for (i in data.indices) {
            sum += data[i]
        }
        sum = sum and 0xffff
        return sum
    }

    fun ota_upgrade() {
        var address = 0x50000
        var remain_size = ota_binary_data.size
        var count = 0
        val page_size = 4096 //default 4096

        // if ecdsa_verify failed restart ota_key_cmd until pass verification.
        ota_key_cmd()
        while (BLEM!!.is_ble_ack == false) {
            ota_key_cmd()
        }
        ota_clearrom_cmd(0x50000, 0x70000)
        while (BLEM!!.is_ble_ack == false) {
            ota_clearrom_cmd(0x50000, 0x70000)
        }
        ota_times = ota_times + 1 //39   //31
        while (remain_size > 0) {
            if (remain_size >= page_size) {
                val tmp = Arrays.copyOfRange(ota_binary_data, count, count + page_size)
                ota_write_page(address, tmp)
                address += page_size
                count = count + page_size
                remain_size -= page_size
            } else {
                val tmp = Arrays.copyOfRange(ota_binary_data, count, count + remain_size)
                ota_write_page(address, tmp)
                address += remain_size
                count = count + remain_size
                remain_size = 0
            }
            progress_count = 100 - remain_size * 100 / ota_binary_data.size

            hdlr.post(Runnable {
            //    TV_verify!!.setText("Times: " + Integer.toString(ota_times) +
              //          "  verify f" + Integer.toString(BLEM!!.verify_fail_count) +
               //         "  clear f:" + Integer.toString(BLEM!!.clear_fail_count))
                update_progress(progress_count)
            })

            Thread.sleep(100)
        }
    }
////////////save file
    fun create_saving_directory() {
        var dataDir = File(storagePath, ECG_DATA_DIRECTORY)
        if(dataDir.mkdirs())Log.e("mkdir", dataDir.toString())
    }

    /*
    fun writeToStorage(fileName: String, data: LongArray) {
        var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$fileName")
        //var outSt = FileOutputStream(extFile, true)
        //val outdata =  tryDiCript(data)
        //outSt.write(data)
        //outSt.close()
        var originalText = ""

        //var regEx =Regex("[a-z]")
        for(tmp in data)
        {
            originalText += tmp.toString() + ","
        }
        originalText.dropLast(1)
        originalText += "\n"
        Log.e("print",originalText)
        extFile.appendText(originalText)
        //extFile
    }

     */
////////////save file


////////////
    fun send_start() {
        if (biologue_char_command != null) {
            biologue_char_command.setValue(COMMAND_ECG_START)
            var ch_cmd = mgatt!!.writeCharacteristic(biologue_char_command)

            if(ch_cmd)Log.e("sendcommand", "start_send")
            else Log.e("sendcommand", "start_sendfailed!!")
        }
        else{
            Log.e("sendcommand", "cmd is null")
        }
    }
    fun send_stop() {

        if (biologue_char_command != null) {
            biologue_char_command.setValue(COMMAND_ECG_STOP)
           var ch_cmd =  mgatt!!.writeCharacteristic(biologue_char_command)
           if(ch_cmd) Log.e("sendcommand", "stop_send")
            else Log.e("sendcommand", "stop_sendfailed!!")
        }

        else{
            Log.e("sendcommand", "cmd is null")
        }
    }

    private val gattCB = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
                gatt: BluetoothGatt,
                status: Int,
                newState: Int
        ) {
            Log.e("GATT", "onConnectStateChange")
            //if() {
            //    Bt_BCG?.text = "connect"
            //    bcg_collect_stage = false
            //}
            gatt?.discoverServices()
        }
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            val intentAction: String
            Log.e("GATT", "onServicesDiscovered")


            biologue_service = gatt!!.getService(UUID.fromString(UUID_SERIVCE))

            //Log.e("Biologue", "Found service")
            // Get Characteristic
            biologue_char_ecg = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_ECG))
            biologue_char_time = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_TIME))

            biologue_char_command = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_COMMAND))

            biologue_char_bcg = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_BCG))

            Log.e("serviceDiscovered", biologue_char_command.toString())

            // Enable Notify ECG
            var notify_success = gatt!!.setCharacteristicNotification(biologue_char_ecg, true)
            if(notify_success) Log.e("Biologue", "Enable notify 1")
            else Log.e("Biologue", "Fail to enable notify 1")

            notify_success = gatt!!.setCharacteristicNotification(biologue_char_bcg, true)
            if(notify_success) Log.e("Biologue", "Enable notify 2")
            else Log.e("Biologue", "Fail to enable notify 2")


            for (dp in biologue_char_ecg.getDescriptors()) {
                Log.e("gattdevice-ecg", "dp:" + dp.toString())
                if (dp != null) {
                    if (biologue_char_ecg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0) {
                        dp.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    }
                    else if (biologue_char_ecg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_INDICATE != 0 ) {
                        dp.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                    }
                    //descript_write = false
                    var tmp = gatt.writeDescriptor(dp)
                    Log.e("ECG", tmp.toString())

                }
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            Log.e("DW", gatt.toString())
            if(!descript_write) {
                for (dp in biologue_char_bcg.getDescriptors()) {
                    Log.e("gattdevice-bcg", "dp:" + dp.toString())
                    if (dp != null) {
                        if (biologue_char_bcg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0) {
                            dp.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                        } else if (biologue_char_bcg.getProperties() != 0 && BluetoothGattCharacteristic.PROPERTY_INDICATE != 0) {
                            dp.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                        }
                        //descript_write = false
                        var tmp = mgatt!!.writeDescriptor(dp)
                        Log.e("BCG", tmp.toString())
                        //Thread.sleep(1000)
                        //send_start()
                        //while(!descript_write){ PERMISSION_REQUEST_STORAGE = PERMISSION_REQUEST_STORAGE }
                    }
                }
            }
            descript_write = true
        }


        override fun onCharacteristicChanged(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?
        ){

            var ecgData = characteristic!!.value
            if (characteristic.uuid == UUID.fromString(UUID_CHAR_ECG)) {

                    var outdata = LongArray(65)

                    outdata.set(0, (ecgData.get(0).toUByte().toLong()
                                 or (ecgData.get(1).toUByte().toLong().shl(8))
                                 or (ecgData.get(2).toUByte().toLong().shl(16))
                                 or (ecgData.get(3).toUByte().toLong().shl(24))))////////timestamp
                var i = 0


                while (i < 64) {
                   //Log.e("ii for", i.toString())
                   var nIndex = (i*2)+4
                   var nIndex2 = nIndex+1
                    i+=1
                    outdata.set(i, ecgData.get(nIndex).toUByte().toLong() or (ecgData.get(nIndex2).toUByte().toLong().shl(8)))/////ecg
                }
                  //  outdata.set(65, ecgData.get(132).toUByte().toLong() or (ecgData.get(133).toUByte().toLong().shl(8)))//////////Temperature
                   // outdata.set(66, ecgData.get(134).toUByte().toLong() or (ecgData.get(135).toUByte().toLong().shl(8)))//////////Checksum

                    //outdata.set(1,ecgData.get(1).toInt())
                    //outdata.set(2,ecg)
                    //presc_cnt++
                    //ecg_data_count++
                    /* Save data */


                var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$DefaultFileName")
                //var outSt = FileOutputStream(extFile, true)
                //val outdata =  tryDiCript(data)
                //outSt.write(data)
                //outSt.close()
                for(i in 1..64) {
                    var originalText = (outdata.get(0)+i).toString() + "   "+ outdata.get(i).toString() +"\n"

                    extFile.appendText(originalText)

                }
                var dff = SimpleDateFormat("MM-dd-HH-mm-ss")
                dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
                var curr_time = dff.format(Date())+ "\n"
                extFile.appendText(curr_time)
                //TV_verify?.setText(outdata.get(0).toString())
                var ecg_cnt = ((outdata.get(0)/64)%100).toInt()
                PB_verifyECG!!.progress = ecg_cnt
                //var regEx =Regex("[a-z]")
                //for(tmp in data)
                //{
                //    originalText += tmp.toString() + ","
                //}
                //originalText.dropLast(1)
                //originalText += "\n"
                //Log.e("print",originalText)
                    ///writeToStorage(DefaultFileName, outdata)

            }
            else if(characteristic.uuid == UUID.fromString(UUID_CHAR_BCG)){

                //if(presc_cnt % PRESC_COUNT == 0) {
                //    Log.e("Biologue", "BCG data in " + ecgData?.size.toString())
                //}
                // txv_record.setText(ecg_data_count.toString())
                // txv_record.setText(characteristic.toString())
                 var j = 0
                var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$DefaultBCGName")
                while(j <6) {


                    var outdata = LongArray(11)
                    var nowIndex = j* 18
                    var nowIndex2 :Int
                    var nowIndex3 :Int

                    outdata.set(0, ecgData.get(nowIndex).toUByte().toLong())///////sync field 85
                    nowIndex += 1
                    outdata.set(1, ecgData.get(nowIndex).toUByte().toLong())///////ID 188

                    nowIndex += 1
                    nowIndex2 = nowIndex +1
                    nowIndex3 = nowIndex +2

///////////////////////////////PreADC
                    outdata.set(2, (ecgData.get(nowIndex).toUByte().toLong() or (ecgData.get(nowIndex2).toUByte().toLong().shl(8)) or (ecgData.get(nowIndex3)).toUByte().toLong().shl(16)))
                    nowIndex +=3
                    outdata.set(3, ecgData.get(nowIndex).toUByte().toLong())///heart rate
                    nowIndex +=1
                    outdata.set(4, ecgData.get(nowIndex).toUByte().toLong())//////Respiratory rate
                    nowIndex +=1
                    outdata.set(5, ecgData.get(nowIndex).toUByte().toLong())//////////signal_status and curve_status
                    nowIndex +=1
                    nowIndex2 = nowIndex +1

                    var minus_test =ecgData.get(nowIndex).toUByte().toLong() or (ecgData.get(nowIndex2).toUByte().toLong().shl(8))
                    if(minus_test > 32767 )minus_test -= 65536
                    outdata.set(6, minus_test)//////////ACC_x
                    // outdata.get(6)

                    nowIndex += 2
                    nowIndex2 = nowIndex +1

                    minus_test = ecgData.get(nowIndex).toUByte().toLong() or (ecgData.get(nowIndex2).toUByte().toLong().shl(8) )
                    if(minus_test > 32767 )minus_test -= 65536
                    outdata.set(7, minus_test)//////////ACC_y

                    nowIndex += 2
                    nowIndex2 = nowIndex +1


                    minus_test = ecgData.get(nowIndex).toUByte().toLong() or (ecgData.get(nowIndex2).toUByte().toLong().shl(8) )
                    if(minus_test > 32767 )minus_test -= 65536
                    outdata.set(8,minus_test)//////////ACC_z

                    nowIndex += 2
                    nowIndex2 = nowIndex +1
                    nowIndex3 = nowIndex +2
////////////////////////////////TimeStamp
                    //outdata.set(9, (ecgData.get(nowIndex).toUByte().toLong() or (ecgData.get(nowIndex2).toUByte().toLong().shl(8)) or (ecgData.get(nowIndex3)).toUByte().toLong().shl(16)))
                    //nowIndex += 3
                    //outdata.set(10, ecgData.get(nowIndex).toUByte().toLong())////////sync field
                    /* Save data */

                    //var outSt = FileOutputStream(extFile, true)
                    //val outdata =  tryDiCript(data)
                    //outSt.write(data)
                    //outSt.close()
                    var originalText = outdata.get(9).toString() + ", " + outdata.get(2).toString() + ", " + outdata.get(6).toString() + ", "
                    originalText += outdata.get(7).toString() + ", " + outdata.get(8).toString() + ", "
                    originalText += (outdata.get(3).toString() + ", ")+ (outdata.get(4).toString() + ", 1\n")

                    extFile.appendText(originalText)
                   // TV_verifyBCG?.setText(originalText)
                    ///////////writeToStorage(DefaultBCGName, outdata)
                    j+=1
                }///////write bcg parced data
                presc_cnt = (presc_cnt % 1000) +1
                //ecg_data_count++
                var presc_cnt2 = presc_cnt/10

                PB_verifyBCG!!.progress = presc_cnt2

                //TV_verifyBCG?.setText(presc_cnt.toString())
                var dff = SimpleDateFormat("MM-dd-HH-mm-ss")
                dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
                var curr_time = dff.format(Date())+ "\n"
                extFile.appendText(curr_time)
            }
        }



        override fun onCharacteristicRead(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?,
                status: Int){


            Log.e("GATT", "READ")

            var recData: ByteArray = characteristic!!.value
            //if(recData.size == 6) {
                var unixTIme: ULong = recData[3].toUByte().toULong().shl(24) + recData[2].toUByte().toULong().shl(16) +
                        recData[1].toUByte().toULong().shl(8) + recData[0].toUByte().toULong()
                Log.e(
                        "data",
                        recData[0].toUByte().toString() + "," + recData[1].toUByte()
                                .toString() + "," + recData[2].toUByte()
                                .toString() + "," + recData[3].toUByte().toString() + "," +
                                recData[4].toUByte().toString() + "," + recData[5].toUByte()
                                .toString() + ","
                )
            //}
        }
        override fun onCharacteristicWrite(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int){
            Log.e("GATT", "WRITE " + characteristic.toString() + status.toString())
        }
    }

    fun update_progress(count: Int) { pg_bar!!.progress = count }
//pg_tv.setText(Integer.toString(count) + "%")
/////button function

    fun fabChoose_Click(view: View) {
        val intent= Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val dest = Intent.createChooser(intent, "Select")
        startActivityForResult(dest, READ_REQUEST_CODE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "load file : ", Toast.LENGTH_SHORT).show()
        if (requestCode == READ_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                val uri = data?.data!!
                val path = uri!!.path
                Toast.makeText(this, "load file : " + get_file_name_from_uri(uri), Toast.LENGTH_SHORT).show()
                val filesize = java.lang.Long.toString(get_file_size_from_uri(uri))
                txv_record.setText("""${get_file_name_from_uri(uri)} size: ${filesize}bytes""")
                try {
                    //String filename = uri.getLastPathSegment();
                    get_binary_data_from_uri(uri)?.let { Log.e(TAG, it) }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun clicktransfer(view: View) {

        if(bluetoothManager.getConnectedDevices(GATT)[CMD_INDEX] == null){
            Toast.makeText(this, "no device selected!!", Toast.LENGTH_LONG).show()
            return
        }
        if( ota_binary_data ==null){
            Toast.makeText(this, "no file selected!!", Toast.LENGTH_LONG).show()
            return
        }

        if (trnsfr_stage){
            Log.e("trnsferring", "stop transfer!!")
            myThread.interrupt()
            if(myThread.isInterrupted){

                Toast.makeText(this, "mythread interrupted!!", Toast.LENGTH_LONG).show()
            }
            BLEM!!.Disconnect()

            Bt_trans?.text = "transfer"
            trnsfr_stage = false

        }
        else {

            var d = bluetoothManager.getConnectedDevices(GATT)[CMD_INDEX]
            Log.e("connection", d.toString())
            BLEM!!.Connect(this, d)
            BLEM!!.connectDevice(d)
            trnsfr_stage = true
            myThread.start()
            //trans_ID = myThread
    //mgatt?.beginReliableWrite()
    //test_char = biologue_service.getCharacteristic(UUID.fromString(UUID_CHAR_ECG))
    //test_char.setValue("testtest123")
    //mgatt?.writeCharacteristic(test_char);

    Bt_trans?.text = "stop"
        }
    }

    fun clickreaddata(view: View) {
        if(bluetoothManager.getConnectedDevices(GATT)[CMD_INDEX2] == null){
            Toast.makeText(this, "no device selected!!", Toast.LENGTH_LONG).show()
            return
        }

       if(ecg_collect_stage){

           if(mgatt!= null) {
               send_stop()/////stop
               mgatt?.close()
           }
           ecg_collect_stage = false

           Bt_ECG?.text = "readecg"

       }


       else{
            val dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            DefaultFileName = dff.format(Date()) + ".txt"
            DefaultBCGName = dff.format(Date()) + "_BCG.txt"
            Log.e("File", DefaultFileName)
            Log.e("File", DefaultBCGName)
           //mgatt = d.connectGatt()

           Thread {

               var d = bluetoothManager.getConnectedDevices(GATT)[CMD_INDEX2]
               mgatt = d.connectGatt(this, false, gattCB)
               Thread.sleep(2000)
               send_start()
           }.start()
           //ECG_ID = Thread.currentThread().id
           //Log.e("ECG", "$ECG_ID")
           //mgatt.writeDescriptor()

           ecg_collect_stage = true
           Bt_ECG?.text = "stopECG"
       }

    }


    fun backtomain(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, 0)

    }
    //button function
}