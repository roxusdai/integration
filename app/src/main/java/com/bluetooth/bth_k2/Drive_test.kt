package com.bluetooth.bth_k2

import android.content.Intent
import android.media.AudioManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.media.SoundPool

class Drive_test : AppCompatActivity() {


/////////////駕駛時間
    var drive_min = 3
/////////////駕駛時間



    var timerHandler3: Handler? = Handler()
    var count_stage =0
    var time_remain = drive_min *60

    lateinit var min_remain_str :String
    lateinit var sec_remain_str :String


    lateinit var txv_timer :TextView

    private val ECG_DATA_DIRECTORY = "test"
    private var storagePath: File? = null
    var stage_change :String = "drive_test_stage_change.csv"

    var snd_pol :SoundPool = SoundPool(1,AudioManager.STREAM_SYSTEM, 5)
    var snd_id :Int = 0;
    private val timerRunnable3: Runnable = object : Runnable {
        override fun run() {
            runOnUiThread {
                if(time_remain>0) {
                    time_remain -= 1
///////////////refresh UI
                    var min_remain = time_remain / 60
                    var sec_remain = time_remain % 60

                    if (min_remain < 10) min_remain_str = "0" + min_remain.toString()
                    else min_remain_str = min_remain.toString()

                    if (sec_remain < 10) sec_remain_str = "0" + sec_remain.toString()
                    else sec_remain_str = sec_remain.toString()

                    txv_timer.text = min_remain_str + ":" + sec_remain_str
                }
///////////////refresh UI
            if(time_remain ==0 && count_stage < 2){

                txv_timer.textSize = 50F
                txv_timer.text = "測試結束,請繼續做PVT測試"



                var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$stage_change")
                val dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
                dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))

                extFile.appendText(dff.format(Date())+"\n")

                snd_pol.play(snd_id, 1F,1F,0,0,1F)
                count_stage = 2
            }////count to zero

            }///////////count time and refresh UI
            timerHandler3!!.postDelayed(this, 1000)

        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drive_test)


        storagePath = this.getExternalFilesDir(null)
        var name = intent.getStringExtra("name")

        //stage_change = intent.getStringExtra("stagechange" )!!
        stage_change = "State_Change_List_"+name+".csv"

        txv_timer = findViewById(R.id.tv_countdown)
        snd_id = snd_pol.load(this,R.raw.blackout,0)

        var min_remain = time_remain/60
        var sec_remain = time_remain%60
        //var min
        if(min_remain<10)min_remain_str = "0" + min_remain.toString()
        else min_remain_str = min_remain.toString()

        if(sec_remain<10)sec_remain_str = "0" + sec_remain.toString()
        else sec_remain_str = sec_remain.toString()

        txv_timer.text = min_remain_str + ":" + sec_remain_str
    }

    fun clickdrive(view: View) {
        if(count_stage ==0) {

            Thread {
                timerHandler3?.postDelayed(timerRunnable3, 0)
            }.start()////////refresh timer thread

            count_stage = 1
        }////start driving
        else if (count_stage == 1){


        }////driving
        else if (count_stage == 2){

        }/////stop driving?
        else{

        }
    }

    fun backtomain(view: View) {
        //finish()
        val intent = Intent(this, MainActivity::class.java)
        //intent.putExtra("mgatt", (Parcelable)mgatt)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent,0)
    }


}