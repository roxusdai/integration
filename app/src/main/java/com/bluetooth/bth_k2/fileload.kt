package com.bluetooth.bth_k2

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.sql.Connection
import java.sql.DriverManager


class fileload : AppCompatActivity() {


    private val DB_URI = ("jdbc:mysql://localhost:3306/ tbl_user_info?"
            + "user=root&password=123456&useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8")

    private val DRIVER = "com.mysql.cj.jdbc.Driver"

    lateinit var uri : Uri
    var filesize: Long = 0
    var fs: FileInputStream? = null

    private lateinit var storagePath: File
    private val ECG_DATA_DIRECTORY = "ECG_DATA"

    @Throws(Exception::class)
    fun connectDB(): Connection? {
        Class.forName(DRIVER)
        return DriverManager.getConnection(DB_URI)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fileload)

        storagePath = Environment.getExternalStorageDirectory()
        var dataDir = File(storagePath, ECG_DATA_DIRECTORY)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e("Permission", "Request External Storage")
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    9
            )
        }


/*
        Thread {
            val startTime = System.nanoTime()
            ota_upgrade()
            Log.e("Measure", "TASK took : " + java.lang.Long.toString((System.nanoTime() - startTime) / 1000000) + "ms")
            //Timestamp_text.setText("Time duration:" + (System.nanoTime() - startTime) / 1000000 + "ms")
        }.start()
*/


    }

    fun clickftpupload(view: View) {

    }//////upload file


    fun clicksendcommand(view: View) {


    }//////send command

    fun fabChoose_Click(view: View) {
        val intent= Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val dest = Intent.createChooser(intent,"Select")
        startActivityForResult(dest,42)

    }//////choose file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        uri = data?.data!!
        val iv = findViewById<TextView>(R.id.textView4)
        if(uri != null)iv.text = uri.toString()

    }

    fun backtomain(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        //intent.putExtra("misc1", misc1)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent)
        //finish()
    }



}