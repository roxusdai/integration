package com.bluetooth.bth_k2;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import static java.lang.String.format;

public class global {
    private static final com.bluetooth.bth_k2.global ourInstance = new com.bluetooth.bth_k2.global();

    public static com.bluetooth.bth_k2.global getInstance() {
        return ourInstance;
    }

    public Context context;

    public com.bluetooth.bth_k2.BLEManager BLEMan;


    void init(Activity activity)
    {
        this.context = activity;

        BLEMan = new com.bluetooth.bth_k2.BLEManager(activity);
    }


    public String DEBUG_LOG = "";
    public static void  Log(final String TAG, final String MSG) {
        Log.i(TAG, MSG);
        Log(TAG + ": " + MSG + "\n");
    }


    /**
     * return String of R.String Res
     */
    public static String getR_String(int id) {
        return com.bluetooth.bth_k2.global.getInstance().context.getResources().getString(id);
    }



    public static void DisplayToast(String Text) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Toast.makeText(com.bluetooth.bth_k2.global.getInstance().context, Text, Toast.LENGTH_LONG).show();
            }
            else
            {
                Log.e("global", format("Toast could not be displayed - not on UI Thread\n'%s'", Text));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void Log(final String MSG) {
        com.bluetooth.bth_k2.global.getInstance().DEBUG_LOG += MSG;
    }
}