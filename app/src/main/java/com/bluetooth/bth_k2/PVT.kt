package com.bluetooth.bth_k2

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class PVT : AppCompatActivity() {
    lateinit var FileName: String
    lateinit var GenderSpin : Spinner
    val genderlist = arrayListOf("男" , "女")
    var Gindex = 0

    lateinit var ET_name:EditText
    lateinit var ET_sleep:EditText
    lateinit var ET_wake:EditText
    lateinit var ET_birth:EditText



    lateinit var cb_License :CheckBox
    lateinit var cb_Caffine :CheckBox
    lateinit var cb_smoke :CheckBox
    lateinit var cb_alchoho :CheckBox
    lateinit var cb_heart :CheckBox

    var license = false
    var caffine = false
    var smoke = false
    var alchoho = false
    var heart = false



    private var storagePath: File? = null
    // private lateinit var today :
    private val ECG_DATA_DIRECTORY = "Profile"


////////////////////save profile
    fun create_saving_directory() {
        var dataDir = File(storagePath, ECG_DATA_DIRECTORY)
        if(dataDir.mkdirs())Log.e("mkdir", dataDir.toString())
    }
////////////save profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p_v_t)


///////edittext

        ET_name = findViewById(R.id.name)
        ET_sleep = findViewById(R.id.sleeptime1)
        ET_wake = findViewById(R.id.waketime2)
        ET_birth = findViewById(R.id.birth)

///////edittext
///////gender spin
        GenderSpin = findViewById(R.id.genderspin)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, genderlist)
        GenderSpin.adapter = adapter
        GenderSpin.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select $p2")
                Gindex = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
///////gender spin

//////////////////////checkbox
        cb_License = findViewById(R.id.CB_License)
        cb_Caffine = findViewById(R.id.CB_caffine)
        cb_smoke = findViewById(R.id.CB_smoke)
        cb_alchoho = findViewById(R.id.CB_alchoho)
        cb_heart = findViewById(R.id.CB_heart)
        val mCheckedListener = CompoundButton.OnCheckedChangeListener{
            buttonView, isChecked->
            when(buttonView.id){
                R.id.CB_License -> {
                    if (isChecked)license = true
                }
                R.id.CB_caffine -> {
                    if (isChecked) caffine = true
                }
                R.id.CB_smoke ->{
                    if (isChecked) smoke = true
                }
                R.id.CB_alchoho ->{
                    if(isChecked)alchoho = true
                }
                R.id.CB_heart ->{
                    if(isChecked)heart = true
                }
            }
        }
        cb_License.setOnCheckedChangeListener(mCheckedListener)
        cb_Caffine.setOnCheckedChangeListener(mCheckedListener)
        cb_smoke.setOnCheckedChangeListener(mCheckedListener)
        cb_alchoho.setOnCheckedChangeListener(mCheckedListener)
        cb_heart.setOnCheckedChangeListener(mCheckedListener)
//////////////checkbox end
    }

    fun submitprofile(view: View) {
        if(ET_name.text.isNullOrEmpty() ||
           ET_birth.text.isNullOrEmpty() ||
           ET_sleep.text.isNullOrEmpty() ||
           ET_wake.text.isNullOrEmpty() ){

            Toast.makeText(this, "Please complete the blank!!", Toast.LENGTH_LONG).show()
        }
        else{
////////////////act time calc

            var totalsleep =ET_wake.text.toString().toInt() - ET_sleep.text.toString().toInt()
            while(totalsleep<0) totalsleep +=12

            val dff1 = SimpleDateFormat("kk")
            val dff2 = SimpleDateFormat("mm")

            dff1.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            dff2.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            val now_hour = dff1.format(Date()).toInt()
            val now_min = dff2.format(Date()).toInt()

            var totalwake =  now_hour  - ET_wake.text.toString().toInt()

////////////////act time calc


////////////////set file path
            storagePath = this.getExternalFilesDir(null)
            create_saving_directory()

            val dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            FileName = "Profile_" + ET_name.text.toString() + "_" + dff.format(Date()) + ".csv"
            Log.e("File", FileName)
/////////////////set file path

/////////////////write data
            var extFile = File(storagePath, "$ECG_DATA_DIRECTORY/$FileName")

            extFile.appendText("device id, "+ Build.MODEL + "\n")
            extFile.appendText("time, " + dff.format(Date())+ "\n")
            extFile.appendText("sleeptime, " + totalsleep.toString()+ "\n")
            extFile.appendText("birth year, " + ET_birth.text.toString()+ "\n")

            if(license)extFile.appendText("license, 1\n")
            else extFile.appendText("license, 0\n")

            if(alchoho)extFile.appendText("drink, 1\n")
            else extFile.appendText("drink, 0\n")

            if(caffine)extFile.appendText("coffine, 1\n")
            else extFile.appendText("coffine, 0\n")

            if(smoke)extFile.appendText("smoke, 1\n")
            else extFile.appendText("smoke, 0\n")

            if(heart)extFile.appendText("cardiac disease, 1\n")
            else extFile.appendText("cardiac disease, 0\n")

            extFile.appendText("in office, 0\n")


            getIntent().putExtra("name",ET_name.text.toString())
            getIntent().putExtra("waketime",ET_wake.text.toString().toInt())
            setResult(RESULT_OK, getIntent())
            finish()
        }

    }

    fun backtomain(view: View) {

        //val intent = Intent(this, MainActivity::class.java)
        //intent.putExtra("misc1", misc1)
        //startActivityForResult(intent,0)
        finish()
    }


}