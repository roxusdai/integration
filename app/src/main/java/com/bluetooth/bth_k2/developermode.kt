package com.bluetooth.bth_k2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class developermode : AppCompatActivity() {
    var misc1:Int = 50
    var textViewMessage: TextView? = null
    var editTextMessage: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_developermode)

        misc1 = intent.getIntExtra("misc1", 50)
        textViewMessage = findViewById(R.id.textView2)
        textViewMessage?.text = "Input:"+ misc1

    }

    fun backtomain(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("misc1", misc1)
        startActivityForResult(intent,0)

    }
}