package com.bluetooth.bth_k2



import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    private lateinit var bManager : BluetoothManager
    private lateinit var bAdapter : BluetoothAdapter
    var ECG_connect = false


    var name :String = "Tester"
    var waketime : Int = 7
    lateinit var stage_change :String



    override fun onRestart(){
        super.onRestart()
        Log.e("onRestart","RE:0")
    }

    private lateinit var txv_time: TextView
    var timerHandler2: Handler? = Handler()
    //val test_transfer = byteArrayOf(0xAA.toByte(),0xF1.toByte(),0xAA.toByte())
    //var tmp1 = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val dff = SimpleDateFormat("yyyy-MM-dd-HH-mm")
        dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))



        //for (tmp2 in test_transfer){
        //    tmp1 += tmp2.toUByte().toString()
       // }

        //Log.e("maintest",tmp1)
        txv_time= findViewById(R.id.TXV_TIME)

        //Start UI Update Timer
        timerHandler2?.postDelayed(timerRunnable2, 0)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        //val builder = AlertDialog.Builder(this)
        //builder.setMessage("Hello")
        //builder.show()

    }

    //////////////////button function
    fun clickBTH(view: View) {
        val intent = Intent(this, bluetooth::class.java)
       //intent.putExtra("mgatt", mgatt)
        startActivityForResult(intent,0)
    }//go to bluetooth connection

    fun clickECG(view: View) {
        val intent = Intent(this, ECG::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        intent.putExtra("ECG_connect", ECG_connect)
        startActivityForResult(intent,1)
    }//go to data collect

    fun clickProfile(view: View) {
        val intent = Intent(this, PVT::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //intent.putExtra("misc1", misc1)
        startActivityForResult(intent,2)
    }/////profile setting


    fun clickRealPVT(view: View) {

        //stage_change = "State_Change_List_"+name+".csv"

        val intent = Intent(this, PVT_menu::class.java)
        intent.putExtra("name", name)
        intent.putExtra("waketime",waketime)
        //intent.putExtra("stagechange" , stage_change)
        startActivityForResult(intent,3)
    }////go to PVT　test
    fun clickDrivetest(view: View) {
        //stage_change = "State_Change_List_"+name+".csv"
        val intent = Intent(this, Drive_test::class.java)

        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("name", name)
        //intent.putExtra("stagechange" , stage_change)
        startActivityForResult(intent,4)

    }///////go to drive test


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 2 && resultCode == RESULT_OK){

          waketime = data!!.getIntExtra("waketime",7)
          name = data.getStringExtra("name")!!
            Log.e("back from profile","waketime:" + waketime.toString())
        }
        else{}
    }


    fun clickUpload(view: View) {
        //val intent = Intent(this, fileload::class.java)
        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //intent.putExtra("misc1", misc1)
        //startActivity(intent)
    }//upload to MySQL


    fun clickAdvoption(view: View) {
       // val intent = Intent(this, developermode::class.java)
        //intent.putExtra("misc1", misc1)
       // startActivityForResult(intent,0)

    }//adv option

    fun clickExit(view: View) {

        moveTaskToBack(true);
        exitProcess(-1)
    }//exit the app
////////////////button function

    private val timerRunnable2: Runnable = object : Runnable {
        override fun run() {
            runOnUiThread {
            val dff = SimpleDateFormat("yyyyMMdd_HH:mm:ss")
            dff.setTimeZone(TimeZone.getTimeZone("GMT+8:00"))
            txv_time.setText(dff.format(Date()))
            }
            timerHandler2!!.postDelayed(this, 1000)
        }
    }




}
